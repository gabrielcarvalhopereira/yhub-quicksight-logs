import logging
import awswrangler as wr
import boto3 as b3
import pandas as pd
from datetime import datetime

session = b3.Session() 
log = logging.getLogger('Processo PROCESSED: ')

s3_location_staging = 's3://yhub-datalakelogs-056220726743-prod/staging/getdashboard_quicksight/'
s3_location_processed = 's3://yhub-datalakelogs-056220726743-prod/processed/getdashboard_quicksight/'

def trata_arquivo_processed(s3_staging_location, session):

    df = wr.s3.read_parquet(path=s3_staging_location, dataset=True, boto3_session=session)
    df = pd.DataFrame(df)    

    df = df.drop_duplicates()    
    df.reset_index(inplace=True, drop=True)

    df['serviceeventdetails'] = df['serviceeventdetails'].astype('str')
    df['dat_ingestao_processed'] = pd.to_datetime(datetime.now())

    df = df.drop_duplicates()    

    return df

def upload_df_particionado(df, diretorio, session):

    wr.s3.to_parquet(df, 
                     diretorio, 
                     boto3_session=session,
                     partition_cols=['ano','mes','dia'],
                     dataset=True
    )
    
def lambda_handler(event, context):
    try:
        log.info('Criando DataFrame para particionar')
        df = trata_arquivo_processed(
            s3_location_staging,
            session
        )
        
        log.info('Particionando e enviando para processed')
        upload_df_particionado(
            df,
            s3_location_processed,
            session
        )
        
    except Exception as ex:
        print(ex.args)
        raise ex
    
def main():
    log.info("Início")

if __name__ == "__main__":
    main()