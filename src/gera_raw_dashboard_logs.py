import logging
import awswrangler as wr
from datetime import datetime, timedelta
import boto3 as b3

session = b3.Session() 
s3_location_raw = 's3://yhub-datalakelogs-056220726743-prod/raw/getdashboard_quicksight/'
database = 'default'

ontem_datetime = datetime.today() - timedelta(days=1)
ontem_date = str(ontem_datetime.date()).replace('-', '/')

log = logging.getLogger("Processo RAW: ")

query_input = '''
    SELECT  
        eventversion,  
        useridentity.type as useridentityType,  
        useridentity.principalid as useridentityPrincipalid,  
        useridentity.arn,  
        useridentity.accountid,  
        useridentity.invokedby,  
        useridentity.accesskeyid,  
        useridentity.username, 
        useridentity.sessioncontext.attributes.mfaauthenticated as useridentitySessioncontextAttributesMfaauthenticated,  
        useridentity.sessioncontext.attributes.creationdate as useridentitySessioncontextAttributesCreationdate,  
        useridentity.sessioncontext.sessionissuer.type as useridentitySessioncontextSessionissuerType,  
        useridentity.sessioncontext.sessionissuer.principalid as useridentitySessioncontextSessionissuerPrincipalid,  
        useridentity.sessioncontext.sessionissuer.arn as useridentitySessioncontextSessionissuerArn,  
        useridentity.sessioncontext.sessionissuer.accountid as useridentitySessioncontextSessionissuerAccountid,  
        useridentity.sessioncontext.sessionissuer.username as useridentitySessioncontextSessionissuerUsername,  
        cast(replace(replace(eventtime,'T',' '),'Z',' ') as timestamp) as eventtime,  
        eventname,  
        awsregion,  
        sourceipaddress,  
        useragent,  
        errorcode,  
        errormessage,  
        requestparameters,  
        responseelements,  
        additionaleventdata,  
        requestid,  
        json_format(cast(resources as json)) as resources,  
        eventtype,  
        apiversion,  
        readonly,  
        recipientaccountid,  
        serviceeventdetails,         
        sharedeventid,  
        vpcendpointid, 
        cast(json_extract(json_extract(json_extract(serviceeventdetails, '$.eventResponseDetails'),'$.dashboardDetails'),'$.dashboardName') as varchar) AS dashboardName, 
        cast(json_extract(json_extract(json_extract(serviceeventdetails, '$.eventResponseDetails'),'$.dashboardDetails'),'$.dashboardId') as varchar) AS dashboardId, 
        timestamp, substr(timestamp,1,4) as ano, substr(timestamp,6,2) as mes, substr(timestamp,9,2) as dia 
    FROM 
        default.cloudtrail_logs_pp 
    WHERE 
        EVENTNAME = 'GetDashboard' AND 
        TIMESTAMP = '{}' AND 
        eventtype like 'AwsServiceEvent'
    '''.format(ontem_date)
     
def gera_df_athena(query_text:str, database_name:str, session_boto):
    df = wr.athena.read_sql_query(query_text,database_name, session_boto)

    return df

def escreve_parquet_s3_from_df( dataframe, s3_location, session_boto ):
    wr.s3.to_parquet(dataframe, s3_location, boto3_session=session_boto, dataset=True) 
    
    
def lambda_handler(event, context):
    try:
        log.info('Gerando DataFrame')
        df = gera_df_athena(
            query_input,
            database,
            session
        )

        log.info('Escrevendo parquet')        

        escreve_parquet_s3_from_df(
            df,
            s3_location_raw,
            session
        )
    except Exception as ex:
        print(ex.args)
        raise ex


def main():
    log.info("Início")


if __name__ == "__main__":
    main()