import logging
import pandas as pd
import awswrangler as wr
import boto3 as b3

session = b3.Session() 
log = logging.getLogger('Processo CONSUMED: ')

s3_location_consumed = 's3://yhub-datalakelogs-056220726743-prod/consumed/getdashboard_quicksight/'
s3_location_processed = 's3://yhub-datalakelogs-056220726743-prod/processed/getdashboard_quicksight/'

def trata_arquivo_consumed(s3_processed_location, session):
    
    df = wr.s3.read_parquet(path=s3_processed_location, dataset=True, boto3_session=session)
    df = pd.DataFrame(df)    

    df = df.drop_duplicates()    

    df_final = df[['username','dashboardId','dashboardName','arn','useridentityPrincipalid','accountid','eventname','eventtime','ano','mes','dia']]
    df_final.columns = ['dsc_email_acesso','id_dashboard','dsc_nome_dashboard','dsc_arn_dashboard','id_principal_identity','id_account','dsc_event','dat_hora_event_time','ano','mes','dia']

    df_final = df_final.drop_duplicates()  

    return df_final

def upload_df_particionado(df, diretorio, session):

    wr.s3.to_parquet(df, 
                     diretorio, 
                     boto3_session=session,
                     partition_cols=['ano','mes','dia'],
                     dataset=True
    )
    
def lambda_handler(event, context):
    try:
        log.info('Criando DataFrame para particionar')
        df = trata_arquivo_consumed(
            s3_location_processed,
            session
        )
        
        log.info('Particionando e enviando para consumed')
        upload_df_particionado(
            df,
            s3_location_consumed,
            session
        )
        
    except Exception as ex:
        print(ex.args)
        raise ex
    
def main():
    log.info("Início")

if __name__ == "__main__":
    main()