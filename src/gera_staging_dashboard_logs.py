import awswrangler as wr
import pandas as pd
from datetime import datetime
import logging
import boto3 as b3

session = b3.Session() 
log = logging.getLogger("Processo STAGING: ")

s3_location_raw = 's3://yhub-datalakelogs-056220726743-prod/raw/getdashboard_quicksight/'
s3_location_staging = 's3://yhub-datalakelogs-056220726743-prod/staging/getdashboard_quicksight/'

def trata_arquivo_staging(s3_raw_location, session):

    log.info('Lendo Parquet na RAW')
    df = wr.s3.read_parquet(path=s3_raw_location, dataset=True, boto3_session=session)
    df = pd.DataFrame(df)     
    
    log.info('Excluindo linhas duplicadas') 
    df = df.drop_duplicates()

    log.info('Setando tipo dos dados')
    df['eventversion'] = df['eventversion'].astype('str')
    df['useridentityType'] = df['useridentityType'].astype('str')
    df['useridentityPrincipalid'] = df['useridentityPrincipalid'].astype('str')
    df['arn'] = df['arn'].astype('str')
    df['accountid'] = df['accountid'].astype('str')
    df['invokedby'] = df['invokedby'].astype('str')
    df['accesskeyid'] = df['accesskeyid'].astype('str')
    df['username'] = df['username'].astype('str')
    df['useridentitySessioncontextAttributesMfaauthenticated'] = df['useridentitySessioncontextAttributesMfaauthenticated'].astype('str')
    df['useridentitySessioncontextAttributesCreationdate'] = df['useridentitySessioncontextAttributesCreationdate'].astype('str')
    df['useridentitySessioncontextSessionissuerType'] = df['useridentitySessioncontextSessionissuerType'].astype('str')
    df['useridentitySessioncontextSessionissuerPrincipalid'] = df['useridentitySessioncontextSessionissuerPrincipalid'].astype('str')
    df['useridentitySessioncontextSessionissuerArn'] = df['useridentitySessioncontextSessionissuerArn'].astype('str')
    df['useridentitySessioncontextSessionissuerAccountid'] = df['useridentitySessioncontextSessionissuerAccountid'].astype('str')
    df['useridentitySessioncontextSessionissuerUsername'] = df['useridentitySessioncontextSessionissuerUsername'].astype('str')
    df['eventtime'] = pd.to_datetime(df['eventtime'])	
    df['eventname'] = df['eventname'].astype('str')
    df['awsregion'] = df['awsregion'].astype('str')
    df['sourceipaddress'] = df['sourceipaddress'].astype('str')
    df['useragent'] = df['useragent'].astype('str')
    df['errorcode'] = df['errorcode'].astype('str')
    df['errormessage'] = df['errormessage'].astype('str')
    df['requestparameters'] = df['requestparameters'].astype('str')
    df['responseelements'] = df['responseelements'].astype('str')
    df['additionaleventdata'] = df['additionaleventdata'].astype('str')
    df['requestid'] = df['requestid'].astype('str')
    df['resources'] = df['resources'].astype('str')
    df['eventtype'] = df['eventtype'].astype('str')
    df['apiversion'] = df['apiversion'].astype('str')
    df['readonly'] = df['readonly'].astype('str')
    df['recipientaccountid'] = df['recipientaccountid'].astype('str')
    df['serviceeventdetails'] = df['serviceeventdetails'].astype('str')
    df['sharedeventid'] = df['sharedeventid'].astype('str')
    df['vpcendpointid'] = df['vpcendpointid'].astype('str')
    df['dashboardName'] = df['dashboardName'].astype('str')
    df['dashboardId'] = df['dashboardId'].astype('str')
    df['timestamp'] = df['timestamp'].astype('str')
    df['ano'] = df['ano'].astype('str')
    df['mes'] = df['mes'].astype('str')
    df['dia'] = df['dia'].astype('str')

    df_obj = df.select_dtypes(['object'])
    df[df_obj.columns] = df_obj.apply(lambda x: x.str.strip())

    df['eventversion'] = df['eventversion'].str.replace('<NA>','')
    df['useridentityType'] = df['useridentityType'].str.replace('<NA>','')
    df['useridentityPrincipalid'] = df['useridentityPrincipalid'].str.replace('<NA>','')
    df['arn'] = df['arn'].str.replace('<NA>','')
    df['accountid'] = df['accountid'].str.replace('<NA>','')
    df['invokedby'] = df['invokedby'].str.replace('<NA>','')
    df['accesskeyid'] = df['accesskeyid'].str.replace('<NA>','')
    df['username'] = df['username'].str.replace('<NA>','')
    df['useridentitySessioncontextAttributesMfaauthenticated'] = df['useridentitySessioncontextAttributesMfaauthenticated'].str.replace('<NA>','')
    df['useridentitySessioncontextAttributesCreationdate'] = df['useridentitySessioncontextAttributesCreationdate'].str.replace('<NA>','')
    df['useridentitySessioncontextSessionissuerType'] = df['useridentitySessioncontextSessionissuerType'].str.replace('<NA>','')
    df['useridentitySessioncontextSessionissuerPrincipalid'] = df['useridentitySessioncontextSessionissuerPrincipalid'].str.replace('<NA>','')
    df['useridentitySessioncontextSessionissuerArn'] = df['useridentitySessioncontextSessionissuerArn'].str.replace('<NA>','')
    df['useridentitySessioncontextSessionissuerAccountid'] = df['useridentitySessioncontextSessionissuerAccountid'].str.replace('<NA>','')
    df['useridentitySessioncontextSessionissuerUsername'] = df['useridentitySessioncontextSessionissuerUsername'].str.replace('<NA>','')
    
    df['eventname'] = df['eventname'].str.replace('<NA>','')
    df['awsregion'] = df['awsregion'].str.replace('<NA>','')
    df['sourceipaddress'] = df['sourceipaddress'].str.replace('<NA>','')
    df['useragent'] = df['useragent'].str.replace('<NA>','')
    df['errorcode'] = df['errorcode'].str.replace('<NA>','')
    df['errormessage'] = df['errormessage'].str.replace('<NA>','')
    df['requestparameters'] = df['requestparameters'].str.replace('<NA>','')
    df['responseelements'] = df['responseelements'].str.replace('<NA>','')
    df['additionaleventdata'] = df['additionaleventdata'].str.replace('<NA>','')
    df['requestid'] = df['requestid'].str.replace('<NA>','')
    df['resources'] = df['resources'].str.replace('<NA>','')
    df['eventtype'] = df['eventtype'].str.replace('<NA>','')
    df['apiversion'] = df['apiversion'].str.replace('<NA>','')
    df['readonly'] = df['readonly'].str.replace('<NA>','')
    df['recipientaccountid'] = df['recipientaccountid'].str.replace('<NA>','')
    df['serviceeventdetails'] = df['serviceeventdetails'].str.replace('<NA>','')
    df['sharedeventid'] = df['sharedeventid'].str.replace('<NA>','')
    df['vpcendpointid'] = df['vpcendpointid'].str.replace('<NA>','')
    df['timestamp'] = df['timestamp'].str.replace('<NA>','')

    df['requestparameters'] = df['requestparameters'].str.replace('null','')
    df['responseelements'] = df['responseelements'].str.replace('null','')   

    df['dashboardName'] = df['dashboardName'].str.replace('"','')   
    df['dashboardId'] = df['dashboardId'].str.replace('"','')   

    df = df.drop_duplicates()

    log.info('Criando data de ingestão')
    df['dat_ingestao_staging'] = pd.to_datetime(datetime.now())

    return df

def upload_df_particionado(df, diretorio, session):

    wr.s3.to_parquet(df, 
                     diretorio, 
                     boto3_session=session,
                     partition_cols=['ano','mes','dia'],
                     dataset=True
    )
    
def lambda_handler(event, context):
    try:
        log.info('Criando DataFrame para particionar')
        df = trata_arquivo_staging(
            s3_location_raw,
            session
        )
        
        log.info('Particionando e enviando para staging')
        upload_df_particionado(
            df,
            s3_location_staging,
            session
        )
        
    except Exception as ex:
        print(ex.args)
        raise ex
    
def main():
    log.info("Início")

if __name__ == "__main__":
    main()